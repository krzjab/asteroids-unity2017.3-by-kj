# Asteroids Unity2017.3 by KJ

This is a Unity 2017.3 project.
It contains a Asteroids game made for Playsoft company during a recruitment test. Made in 3 days.

# 2018-10-10 Notatka[PL]:
Działanie gry w wersji na Androida zostało sprawdzone w 4 rozdzielczościach:
1920x1080 , 1280x720 landescape oraz 1080x1920, 720x1280 portrait. Do tych rozdzielczości na smartphonie/Androidzie gra powinna się dostosowywać.

Poza tym wygenerowałem build na win/linux/Mac, był sprawdzany na win7 w rozdzielczościach 1920x1080, 1280x720. 

W innych rozdzielczościach z grubsza powinno działać, ale GUI mogą się rozjeżdżać.
W wersji na androida orientacja GUI nie powinna zmieniać się dynamicznie w trakcie zmiany orientacji ekranu.
​
Build gry na Androida: https://www.dropbox.com/s/ivjud1evb7nl9sq/asteroid-KJ-app-TestingOn_v02.apk?dl=0

Build na PC, sprawdzane na windows7(sterowanie klawiaturą, strzałkami i spację): https://www.dropbox.com/s/bsl5tm9580j4ji9/build-pc-kj-asteroids.rar?dl=0


