﻿#define TestAsteroids

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class AsteroidSpawner : MonoBehaviour {

	public List<GameObject> asteroids = new List<GameObject>();
	AsteroidFactory aFactory = new AsteroidFactory();

	[SerializeField]
	private GameObject smallAstroidPrefForIns;
	public static GameObject smallAsteroidPrefab;
	[SerializeField]
	private GameObject mediumAstroidPrefForIns;
	public static GameObject mediumAsteroidPrefab;
	[SerializeField]
	private GameObject bigAstroidPrefForIns;
	public static GameObject bigAsteroidPrefab;

	private float timeToSpawn;
	private float currentTime;
	bool asteroidsSpawningEnabled;

	private void Awake()
	{
		smallAsteroidPrefab = smallAstroidPrefForIns;
		mediumAsteroidPrefab = mediumAstroidPrefForIns;
		bigAsteroidPrefab = bigAstroidPrefForIns;

		#if !TestAsteroids
		asteroids.Add(aFactory.GetAsteroid(AsteroidType.Big));
		asteroids.Add(aFactory.GetAsteroid(AsteroidType.Medium));
		asteroids.Add(aFactory.GetAsteroid(AsteroidType.Small));
		#endif
		timeToSpawn = GameMaster.asteroidSpawnTime; // seconds
		currentTime = 0.0f;



	}

	// Use this for initialization
	void Start () {
		asteroidsSpawningEnabled = true;
	}

	public void Spawn2Asteroids(AsteroidType asterType, Vector2 asterPos, Vector2 asterVelo)
	{
		if (asterType != AsteroidType.Non) {
			Vector2 asterVelo1 = Rotate (asterVelo, 45.0f);
			Vector2 asterVelo2 = Rotate (asterVelo, -45.0f);

			asteroids.Add (aFactory.GetAsteroid (asterType, asterPos, asterVelo1));
			asteroids.Add (aFactory.GetAsteroid (asterType, asterPos, asterVelo2));
		}
	}

	public Vector2 Rotate(Vector2 v, float degrees)
	{
		float radians = degrees * Mathf.Deg2Rad;
		float sin = Mathf.Sin (radians);
		float cos = Mathf.Cos (radians);
		float tempX = v.x;
		float tempY = v.y;
		return new Vector2(cos * tempX - sin * tempY, sin * tempX + cos*tempY);
	}

	// Update is called once per frame
	void Update () {

		#region spawn asteroids in time intervals
		if (asteroidsSpawningEnabled == true){
			currentTime += Time.deltaTime;
			if (currentTime >= timeToSpawn) 
			{
				currentTime = 0.0f;
				float randomAsteroidCheck = Random.Range (0.0f, 1.0f);
				if ( randomAsteroidCheck <= 0.33f)
				{
					asteroids.Add(aFactory.GetAsteroid(AsteroidType.Big));
				}
				if ( randomAsteroidCheck > 0.33f && randomAsteroidCheck <= 0.66f)
				{
					asteroids.Add(aFactory.GetAsteroid(AsteroidType.Medium));
				}
				if ( randomAsteroidCheck > 0.66f && randomAsteroidCheck <= 1.0f)
				{
					asteroids.Add(aFactory.GetAsteroid(AsteroidType.Small));
				}

			}
		}
		#endregion
	}

	public void RemoveAsteroidFromList( GameObject asteroidGO)
	{
		foreach (GameObject aster in asteroids) {
			if (asteroidGO.GetInstanceID() == aster.GetInstanceID()) {
				asteroids.Remove (aster); // remove asteroid from the list 
				break;
			}
		}
	}

	public void DestroyAllAsteroids()
	{
		#region block asteroids spawning
		asteroidsSpawningEnabled = false;
		#endregion



		#region destroy asteroids 

		foreach (GameObject asteroid in asteroids)
		{
			
			asteroid.GetComponent<Asteroid>().DestroyAster();

		}


		#endregion
		//clear asteroids list
		asteroids.Clear();
	}

	public void EnableSpawning()
	{
		asteroidsSpawningEnabled = true;
	}

	public enum AsteroidType {Big, Medium, Small, Non};

	public class AsteroidFactory
	{
		public GameObject GetAsteroid( AsteroidType enumType)
		{
			GameObject asteroidTemp;
			Vector3 asteroidSpawnPoint = Vector3.zero;

			#region CALCULATE asteroid spawn point with a Random on the edges
			//get seed for the random asteroid spawn point
			Random.InitState (Random.Range ((int)0.0, (int)1000.0));

			float verticalOrHorizontalSpawn = Random.Range (0.0f, 1.0f);
			//randomize the position
			if (verticalOrHorizontalSpawn < 0.5f) {
				//spawn on left or right edge
				asteroidSpawnPoint = Camera.main.ViewportToWorldPoint (new Vector3 ((float)Random.Range ((int)0.0, (int)2.0), Random.Range (0.0f, 1.0f), 1.0f));
			} else if ( verticalOrHorizontalSpawn >= 0.5f)
			{
				//spawn on upper or lower edge
				asteroidSpawnPoint = Camera.main.ViewportToWorldPoint (new Vector3 ((float)Random.Range (0.0f, 1.0f), Random.Range ((int)0.0, (int)2.0), 1.0f));
			}
			#endregion

			switch (enumType) 
			{
				case AsteroidType.Small:
					//instanciate prefab
					asteroidTemp = (GameObject)Instantiate( smallAsteroidPrefab, asteroidSpawnPoint, Quaternion.identity) as GameObject;
					


					
				//	missile.GetComponent<MissileBase> ().SetMissileVelocity (v2_missileSpeed);
					//return asteroidTemp;
					break;
				case AsteroidType.Medium:
					//instaciate
					asteroidTemp = (GameObject)Instantiate( mediumAsteroidPrefab, asteroidSpawnPoint, Quaternion.identity) as GameObject;
					//return asteroidTemp;
					break;
					
				case AsteroidType.Big:
						//instanciate 
					asteroidTemp = (GameObject)Instantiate( bigAsteroidPrefab, asteroidSpawnPoint, Quaternion.identity) as GameObject;
					//return asteroidTemp;
					break;
					
				default:
						//instanciate 
					asteroidTemp = (GameObject)Instantiate( smallAsteroidPrefab, asteroidSpawnPoint, Quaternion.identity) as GameObject;
					//return asteroidTemp;
					break;
					



			}

			#region add velocity to asteroid and return it asteroid in the method
			Vector2 velocityDir;
			Vector2 targetPos;
			targetPos = Camera.main.ViewportToWorldPoint (new Vector3 (Random.Range (0.25f, 0.75f), Random.Range (0.25f, 0.75f), 1.0f));
			velocityDir = (targetPos - (Vector2)asteroidTemp.transform.position).normalized;
			float velocityMagnitude = Random.Range(GameMaster.astrVeloMin, GameMaster.astrVeloMax);
			asteroidTemp.GetComponent<Rigidbody2D>().velocity = velocityDir * velocityMagnitude;

			return asteroidTemp;
			#endregion



		}

		public GameObject GetAsteroid( AsteroidType enumType, Vector2 asteroidPos, Vector2 velocity)
		{
			GameObject asteroidTemp;


			switch (enumType) 
			{
			case AsteroidType.Small:
				//instanciate prefab
				asteroidTemp = (GameObject)Instantiate( smallAsteroidPrefab, asteroidPos, Quaternion.identity) as GameObject;

				break;
			case AsteroidType.Medium:
				//instaciate
				asteroidTemp = (GameObject)Instantiate( mediumAsteroidPrefab, asteroidPos, Quaternion.identity) as GameObject;

				break;

			case AsteroidType.Big:
				//instanciate 
				asteroidTemp = (GameObject)Instantiate( bigAsteroidPrefab, asteroidPos, Quaternion.identity) as GameObject;

				break;

			default:
				//instanciate 
				asteroidTemp = (GameObject)Instantiate( smallAsteroidPrefab, asteroidPos, Quaternion.identity) as GameObject;
				//return asteroidTemp;
				break;




			}

			#region add velocity to asteroid and return asteroid in the method

			asteroidTemp.GetComponent<Rigidbody2D>().velocity = velocity;

			return asteroidTemp;
			#endregion



		}
	}
}
