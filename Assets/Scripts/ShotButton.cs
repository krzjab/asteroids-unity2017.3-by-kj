﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotButton : ButtonScr {


	protected override void Start()
	{
		base.Start();
		transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.80f, 0.20f, 1.0f));
		
	}

	protected override void OnMouseEnter()
	{
		
	}

	protected override void OnMouseDown()
	{
		buttonSprite.color = Color.red;
		buttonSprite.color = new Color (buttonSprite.color.r, buttonSprite.color.g, buttonSprite.color.b, 0.33f);

	}

	protected override void OnMouseExit()
	{
		//buttonSprite.color = Color.white;
		//buttonSprite.color = new Color (buttonSprite.color.r, buttonSprite.color.g, buttonSprite.color.b, 0.33f);
		//InputMaster.shoot = false;
	}

	protected override void OnMouseUpAsButton()
	{
		InputMaster.shoot = true;
		buttonSprite.color = Color.white;
		buttonSprite.color = new Color (buttonSprite.color.r, buttonSprite.color.g, buttonSprite.color.b, 0.33f);
	}
}
