﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMedium : Asteroid {

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		asterTypeToSpawn = AsteroidSpawner.AsteroidType.Small;
	}
	
	// Update is called once per frame
	protected override void FixedUpdate () {
		base.FixedUpdate ();
	}
}
