﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowButton : MonoBehaviour {


	protected SpriteRenderer buttonSprite;


	// Use this for initialization
	protected virtual void Start () {
		buttonSprite = gameObject.GetComponent<SpriteRenderer> ();

	}


	protected virtual void OnMouseEnter()
	{
		buttonSprite.color = Color.red;
		buttonSprite.color = new Color (buttonSprite.color.r, buttonSprite.color.g, buttonSprite.color.b, 0.33f);
	}

	protected virtual void OnMouseOver()
	{
		InputChange (true);
	}
	protected virtual void OnMouseExit()
	{
		buttonSprite.color = Color.white;
		buttonSprite.color = new Color (buttonSprite.color.r, buttonSprite.color.g, buttonSprite.color.b, 0.33f);
		InputChange (false);
	}

	protected virtual void InputChange(bool isActive)
	{
		//InputMaster.up = isActive;
		InputMaster.up = isActive;
	}

}
