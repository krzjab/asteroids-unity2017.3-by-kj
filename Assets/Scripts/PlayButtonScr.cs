﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayButtonScr : ButtonScr {

	protected override void OnMouseUpAsButton()
	{
		// what does the button do?
		SingletonScoreManager.score = 0;
		SceneManager.LoadScene (1); // enter the first gameplay scene
	}


		
}
