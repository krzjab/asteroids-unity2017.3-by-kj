﻿
using UnityEngine.SceneManagement;

public class ExitButtonScr : ButtonScr {

	protected override void OnMouseUpAsButton()
	{
		// what does the button do?
		SceneManager.LoadScene (0); // enter the first gameplay scene
	}
}
