﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SFX_types {shot, explosion, death, respawn}
public enum Music_types {menuMusic, gameplayMusic}

public class AudioMaster : MonoBehaviour {

	//Sttic singleton property
	public static AudioMaster Instance {get; private set;} // instance of score manager

	[SerializeField]
	private AudioSource musicSource;
	[SerializeField]
	private AudioSource sfxSource;
	[SerializeField]
	private AudioClip[] audioClips;



	void Awake()
	{
		musicSource = gameObject.transform.Find ("Music").GetComponent<AudioSource> ();
		sfxSource = gameObject.transform.Find ("SFXs").GetComponent<AudioSource> ();

		//Load Sounds and Music to audioclips array
		audioClips = new AudioClip[6];
		//shot SFX
		audioClips[0] = Resources.Load("ie_shot_gun-luminalace-770179786") as AudioClip;
		//explosion SFX
		audioClips[1] = Resources.Load("44 Magnum-SoundBible.com-162460153") as AudioClip;
		//death SFX
		audioClips[2] = Resources.Load("Plastic_Bottle_Crush-Simon_Craggs-1401313467") as AudioClip;
		//respawn SFX
		audioClips[3] = Resources.Load("Power-Up-KP-1879176533") as AudioClip;
		//menu music, game over music
		audioClips[4] = Resources.Load("dawnofwar") as AudioClip;
		//gameplay music
		audioClips[5] = Resources.Load("carnival") as AudioClip;

		#region singleton stuff
		//Check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			//Detroy other instances if they are not the same
			Destroy(gameObject);
		}

		//Save our current singleton instance
		Instance = this;
		#endregion
	}



	public void PlaySFX(SFX_types sfx)
	{
		AudioClip aClipTemp = null;
		switch (sfx) {
			case SFX_types.shot:
				aClipTemp = audioClips [0];
				break;
			case SFX_types.explosion:
				aClipTemp = audioClips [1];
				break;
			case SFX_types.death:
				aClipTemp = audioClips [2];
				break;
			case SFX_types.respawn:
				aClipTemp = audioClips [3];
				break;
		}
		sfxSource.PlayOneShot (aClipTemp);
	}

	public void PlayMusicLoop(Music_types mType)
	{
		AudioClip aClipTemp = null;;
		switch(mType)
		{
			case Music_types.menuMusic:
				aClipTemp = audioClips [4];
				break;
			case Music_types.gameplayMusic:
				aClipTemp = audioClips [5];
				break;
		}

		musicSource.loop = true;
		musicSource.clip = aClipTemp;
		musicSource.Play ();

	}
}
