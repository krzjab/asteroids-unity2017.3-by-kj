﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : DynamicObject,IHitable {


	protected GameObject aSpawnerGO;
	protected Rigidbody2D asterRig;
	protected float velReductionAfterHit; // percent amount of original velocity which goes to new asteroids on hit
	protected AsteroidSpawner.AsteroidType asterTypeToSpawn;

	// Use this for initialization
	protected virtual void Start () {
		gameObject.GetComponent<Collider2D> ().isTrigger = true;
		aSpawnerGO = GameObject.Find ("AsteroidSpawner");
		asterRig = gameObject.GetComponent<Rigidbody2D> ();
		velReductionAfterHit = GameMaster.velReductionAfterHit;
		asterTypeToSpawn = AsteroidSpawner.AsteroidType.Small;

	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate ();
	}

	public void GotHit()
	{
		if (aSpawnerGO != null) {
			aSpawnerGO.GetComponent<AsteroidSpawner> ().RemoveAsteroidFromList (gameObject);
		} else {
			Debug.Log (" Asteroid Spawner Reference object missing, fix the Find method. Mayby Asteroid SPawner GO is missing in the scene.");
		}
			
		aSpawnerGO.GetComponent<AsteroidSpawner> ().Spawn2Asteroids (asterTypeToSpawn, (Vector2)gameObject.transform.position, asterRig.velocity * velReductionAfterHit);

		SingletonScoreManager.Instance.AddPoints ((int)GameMaster.scorePerAsteroid);

		Destroy (this.gameObject);
	}

	public virtual void DestroyAster()
	{
		Destroy (this.gameObject);
	}

	protected virtual void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") {
			col.gameObject.GetComponent<IHitable> ().GotHit ();
			FXMaster.Instance.SpawnFX (FX_types.explosion, col.transform.position, col.transform.rotation.eulerAngles);
			AudioMaster.Instance.PlaySFX (SFX_types.explosion);
		}
	}

}
