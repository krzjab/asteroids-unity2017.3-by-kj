﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpButton : ArrowButton {

	protected override void InputChange(bool isActive)
	{
		InputMaster.up = isActive;
	}

	protected override void Start()
	{
		base.Start ();
		transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.15f, 0.19f, 1.0f));
	}

}
