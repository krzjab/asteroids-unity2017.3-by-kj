﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class used to adjust gameplay assets to the current resolution
public class ResolutionMaster : MonoBehaviour {

	/// <summary>
	/// Class sets up 4 resoloutions on Awake. 
	/// 1080x1920 and 720x1280 desktop resolutions (landscapes orientation on mobiles?).
	/// 1920x1080 and 1280x720 portrait resolution on smartphones/mobiles .
	/// Does not update GUI while changing between landscapes and portrait orientations.
	/// GUI is set for the first orientation.
	/// </summary>

	void Awake()
	{
			
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor) {
			if ((Screen.height == 1280 && Screen.width == 720) || (float)(Screen.height / Screen.width) == (float)1280/720 ) {
				//adjust in game assets
				Camera.main.orthographicSize = 10;
				//Debug.Log("GameWindow resolution ratio is: " + (float)1280/720);
				GameObject.Find ("Background").transform.localScale = new Vector3 (3.0f, 3.0f, 1);
			}

			if ((Screen.height == 1920 && Screen.width == 1080) || (float)(Screen.height / Screen.width) == (float)1920/1080 ) {
				//adjust in game assets
				Camera.main.orthographicSize = 10;
				Debug.Log("GameWindow resolution ratio is: " + (float)1920/1080);
				GameObject.Find ("Background").transform.localScale = new Vector3 (3.0f, 3.0f, 1);
			}

			if ((Screen.height == 1080 && Screen.width == 1920) || (float)(Screen.height / Screen.width) == (float)1080/1920) {
				Camera.main.orthographicSize = 7;
				//Camera.main.orthographicSize = 5;
				GameObject.Find ("Background").transform.localScale = new Vector3 (1.4f, 1.4f, 1);
			}
			if ((Screen.height == 720 && Screen.width == 1280) || (float)(Screen.height / Screen.width) == (float)720/1280) {
				Camera.main.orthographicSize = 7;
				//Camera.main.orthographicSize = 5;
				GameObject.Find ("Background").transform.localScale = new Vector3 (1.4f, 1.4f, 1);
			}
		}
	}

	void Update()
	{
		//Debug.Log ("Curren Screen Resolution Size is height:" + Screen.currentResolution.height + " and width: " + Screen.currentResolution.width);
		//Debug.Log ("Curren Resolution of screen window is height:" + Screen.height + " and width: " + Screen.width);

	
	}

}
