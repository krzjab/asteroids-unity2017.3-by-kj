﻿#define SwitchScenes
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour {

		#region Constants for GameDesigner !!!!!!!!!!!!!!!
		public const int maxLives = 3;
		public const float shipMaxSpeed = 8.0f;
		public const int scorePerAsteroid = 10;
		public const float asteroidSpawnTime = 3.0f; //asteroids are spawning in time intervals
		public const float velReductionAfterHit = 0.9f; // fraction of velocity of new spawned asteroids after a asteroid hit, Example: if 0.90 new asteroid has 90% velocity of the destroyed asteroid
		public const float astrVeloMin = 3.0f; // asteroids minimal velocity on spawning from AsteroidSpawner
		public const float astrVeloMax = 5.0f; // asteroids maximal velocity on spawning from AsteroidSpawner
		public const float timeToDestroyMissile = 2.0f; // float -> seconds
		public const float missileVelocity = 11.0f; // velocity of new spaned missiles which are spawned by playerShip
		public const float rotationSpeed = 200; // rotation speed of playerShip
		#endregion

		public GameObject inputMasterGO;
		public GameObject asteroidSpawnerGO;
		
		public GameObject shipPrefab; // add ship prefab in inspector
		public GameObject livePrefab; // add a prefab to inspector
		
		int currentLives;
		public List<GameObject> lives = new List<GameObject>();


		float timeToSceneLoad;
		float currentSceneTimer;
		bool respawnTrigger;
		float respawnTimer;
		float currentRespawnTimer;

		// Use this for initialization
		void Start () 
		{
			AudioMaster.Instance.PlayMusicLoop (Music_types.gameplayMusic);
			SingletonScoreManager.score = 0;
			
			respawnTimer = 2.0f;
			currentRespawnTimer = 0.0f;
			respawnTrigger = false;


			timeToSceneLoad = 30.0f;
			currentSceneTimer = 0.0f;
			currentLives = maxLives;

			for (int i = 0;  i < maxLives; i++) {

				float toRightMove; // move each live little to right on spawning
				toRightMove = 0.05f * i;
				Vector2 liveSpawnPoint;
				liveSpawnPoint = (Vector2) Camera.main.ViewportToWorldPoint (new Vector3 (0.03f + toRightMove , 0.9f, 1.0f));
				GameObject live;
				live	= (GameObject)Instantiate (livePrefab, liveSpawnPoint, Quaternion.Euler(new Vector3(0.0f,0.0f,90.0f))) as GameObject;
				lives.Add (live);
			}

			inputMasterGO = GameObject.Find ("InputMaster");
			asteroidSpawnerGO = GameObject.Find ("AsteroidSpawner");
			

		}

		public void LoseLiveUI()
		{
			if (currentLives > 1) {
				lives [currentLives - 1].GetComponent<SpriteRenderer> ().enabled = false;
				currentLives--;
				respawnTrigger = true;
				#region destroy asteroids
				asteroidSpawnerGO.GetComponent<AsteroidSpawner> ().DestroyAllAsteroids ();
				#endregion
				AudioMaster.Instance.PlaySFX (SFX_types.death); //SoundEffect
			} else if (currentLives == 1) {
				GameOver ();
			}
			
		}

		private void GameOver()
		{
			if (SingletonScoreManager.highScore < SingletonScoreManager.score) {
				SingletonScoreManager.highScore = (int)SingletonScoreManager.score;
				SingletonScoreManager.SaveScore ();
			}
			SceneManager.LoadScene (2);//load GameOver Scene

		}

		// Update is called once per frame
		void Update () {
			#region respawn ship after death trigger On asteroid spawning
			if (respawnTrigger == true)
			{
				currentRespawnTimer += Time.deltaTime;
				if (currentRespawnTimer >= respawnTimer)
				{
					respawnTrigger = false;
					currentRespawnTimer = 0.0f;
					Vector2 screenCenter = (Vector2)Camera.main.ViewportToWorldPoint(new Vector3(0.5f,0.5f,1.0f));
					GameObject ship;
					ship = (GameObject)Instantiate (shipPrefab, screenCenter, Quaternion.Euler(new Vector3(0.0f,0.0f,90.0f))) as GameObject;
					inputMasterGO.GetComponent<InputMaster>().GetShip();
					asteroidSpawnerGO.GetComponent<AsteroidSpawner>().EnableSpawning();
					AudioMaster.Instance.PlaySFX (SFX_types.respawn); //SoundEffect
				}
			}
			#endregion

			#if !SwitchScenes
				#region change scenes in time MADE TO CHECK the singleton, I had to gameplay scenes to switch between
				currentSceneTimer += Time.deltaTime;
				if (currentSceneTimer >= timeToSceneLoad) {

					Scene currentScene = SceneManager.GetActiveScene ();
					int buildIndex = currentScene.buildIndex;

					switch (buildIndex)
					{
						case 0:
							SceneManager.LoadScene((int)1.0);
							break;
						case 1:
							SceneManager.LoadScene((int)0.0);
							break;
					}
				}

				#endregion
			#endif
		}

}

