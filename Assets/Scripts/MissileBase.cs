﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBase : DynamicObject {

	//Script for missileStar hazard. Includes influence on player character.
	protected Vector2 v2_missleSpawnVelocity;
	protected bool velocityChangeBool;
	protected float timeToDestroy;
	protected Rigidbody2D missile_rig;
	bool changeMissileVelocity;

	protected virtual void OnTriggerEnter2D(Collider2D col )
	{
		
		if (col.tag == "Player" || col.tag == "Enemy") {
			col.gameObject.GetComponent<IHitable> ().GotHit ();

			AudioMaster.Instance.PlaySFX (SFX_types.explosion);
			FXMaster.Instance.SpawnFX (FX_types.explosion, col.transform.position, col.transform.rotation.eulerAngles);

			Destroy (gameObject);
		}
	

	}//OnTriggerStay2D



	// Use this for initialization
	protected void Start () {
		changeMissileVelocity = true;
		timeToDestroy = GameMaster.timeToDestroyMissile;
		StartCoroutine (DestroyMissile(timeToDestroy));
		missile_rig = gameObject.GetComponent<Rigidbody2D> ();
	
	}

	void Update()
	{
		if (changeMissileVelocity) {
			missile_rig.velocity = v2_missleSpawnVelocity;
			changeMissileVelocity = false;
		}
	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate ();
	}
		
	public virtual void SetMissileVelocity(Vector2 vec2)
	{
		v2_missleSpawnVelocity = new Vector2 (vec2.x, vec2.y);
	}

	protected IEnumerator DestroyMissile(float destroySpeed)
	{
		yield return new WaitForSeconds (destroySpeed);
		Destroy (this.gameObject);
	}



}
