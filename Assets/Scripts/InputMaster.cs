﻿//#define TestingAndroidOnWin //keyboard working for PC Linux and Mac if commented
#define EnableButtonsEveryTime

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputMaster : MonoBehaviour {

	public static bool up; // for ship acceleration in the asteroid game
	public static bool left; // for rotating left in the asteroid game
	public static bool right; // for rotating right in the asteroid game
	public static bool shoot; // to shoot missiles in the asteroid game, space button on PC or shoot virtual button on a Mobile version
	public GameObject playerShip;
			
	public bool disableKeyboard;
	GameObject[] mobileButtons;

		// Use this for initialization
	void Start () {
		#if TestingAndroidOnWin
			disableKeyboard = true;
		#endif
		GetShip ();

		up = left = right = shoot = false;

		EnableButtonsOnAndroid ();
	}

	void EnableButtonsOnAndroid()
	{
		
		mobileButtons = GameObject.FindGameObjectsWithTag ("MobileButton");


		if (Application.platform == RuntimePlatform.Android) {

			#region enable arrows and shoot buttons on Android
			foreach (GameObject button in mobileButtons)
			{
				button.SetActive(true);
				button.GetComponent<Collider2D>().enabled = true;
				button.GetComponent<SpriteRenderer>().enabled = true;
			}

			#endregion
		
		}
		#if EnableButtonsEveryTime
			#region enable arrows and shoot buttons on Android
			foreach (GameObject button in mobileButtons)
			{
				button.SetActive(true);
				button.GetComponent<Collider2D>().enabled = true;
				button.GetComponent<SpriteRenderer>().enabled = true;
			}

			#endregion
		#endif

		#if !TestingAndroidOnWin
			#if UNITY_STANDALONE || UNITY_EDITOR

				#region DISABLE arrows and shoot buttons, u will use keyboard on Win Linux and Mac
				foreach (GameObject button in mobileButtons)
				{
					button.SetActive(false);
					button.GetComponent<Collider2D>().enabled = false;
					button.GetComponent<SpriteRenderer>().enabled = false;
				}
				#endregion
			#endif
		#endif
	}

	public void GetShip()
	{
		playerShip = GameObject.FindWithTag ("Player"); // find player ship game object
		if (playerShip == null) {
			Debug.Log ("Player Ship not found in the Input Master");
		}
	}

	// Update is called once per frame
	void Update () {
		
		#if UNITY_STANDALONE || UNITY_EDITOR
			if (disableKeyboard == false) {
				#region PC input
				
				if (Input.GetKey (KeyCode.UpArrow)) {
					up = true;
				} else {
					up = false;
				}
				if (Input.GetKey (KeyCode.LeftArrow)) {
					left = true;
				} else {
					left = false;
				}
				if (Input.GetKey (KeyCode.RightArrow)) {
					right = true;
				} else {
					right = false;
				}
				if (Input.GetKeyDown (KeyCode.Space)) {
					shoot = true;
				} else {
					shoot = false;
				}

				#endregion
			}
		#endif

 	}

	void FixedUpdate()
	{
		
		if (playerShip != null) {
			playerShip.GetComponent<PlayerShip> ().Move (up); // send input to Player SHip acceleration engine
			if (shoot) {
				playerShip.GetComponent<PlayerShip> ().SpawnMissleFunction (); // Shot missile
				shoot = false;
			}
		}

	}

	//------------------------ character controlll base class
	

}


