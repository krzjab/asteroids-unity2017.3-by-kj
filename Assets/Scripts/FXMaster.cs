﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FX_types {explosion, shotExplosion}

public class FXMaster : MonoBehaviour {

	//Sttic singleton property
	public static FXMaster Instance {get; private set;} // instance of score manager

	[SerializeField]
	private GameObject exploPref; //insert in inspector
	[SerializeField]
	private GameObject shotExploPref; // insert in inspector

	void Awake()
	{
		
		#region singleton stuff
		//Check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			//Detroy other instances if they are not the same
			Destroy(gameObject);
		}

		//Save our current singleton instance
		Instance = this;
		#endregion
	}



	public void SpawnFX(FX_types sfx, Vector2 fxPos, Vector3 fxOrientationInEulerAngles)
	{
		GameObject fxTemp = null;
		switch (sfx) {
		case FX_types.explosion:
			fxTemp = exploPref;

			break;
		case FX_types.shotExplosion:
			fxTemp = shotExploPref;
			break;
		}
		GameObject fx;
		fx = (GameObject)Instantiate(fxTemp,fxPos,Quaternion.Euler(fxOrientationInEulerAngles)) as GameObject;

	}

	public void SpawnFX(FX_types sfx, Vector2 fxPos, Vector3 fxOrientationInEulerAngles, Transform fxParent)
	{
		GameObject fxTemp = null;
		switch (sfx) {
		case FX_types.explosion:
			fxTemp = exploPref;

			break;
		case FX_types.shotExplosion:
			fxTemp = shotExploPref;
			break;
		}
		GameObject fx;
		fx = (GameObject)Instantiate(fxTemp,fxPos,Quaternion.Euler(fxOrientationInEulerAngles), fxParent ) as GameObject;

	}
		
}