﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftButton : ArrowButton {

	protected override void InputChange(bool isActive)
	{
		InputMaster.left = isActive;
	}

	protected override void Start()
	{
		base.Start ();
		transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.09f, 0.10f, 1.0f));
	}
}
