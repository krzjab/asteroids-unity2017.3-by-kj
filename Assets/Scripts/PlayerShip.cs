﻿#define DecelerationLessInertia

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerShip : DynamicObject,IHitable {

	[SerializeField] public float current_MaxSpeed = 8f;                    // The fastest the character can travel in the x axis.

	protected Rigidbody2D m_Rigidbody2D;
	private GameMaster gm;

	public float VelocityX_look;
	public float VelocityY_look;

	public float acelerationNr1Time; //Firs aceleration time segment
	public float acelerationNr2Time; //Sceond aceleartion time segment, character gets momentum
	public float decelerationTime; //Deceleartion Time segment

	public float Velocity_minPercent; //  percent of the maximum speed for min speed
	public float Velocity_firstSegmentPart; // percent of the maximum speed  for the first segment speed

	public float acelerationNr1; // aceleration to calculate from time and Velocity
	public float acelerationNr2; // aceleration to calculate from time and velocity

	private Vector2 missileSpawnPoint;
	private GameObject missileSpawnPointGO;
	private Vector2 v2_missileSpeed;
	private float missileVelocity;
	public GameObject missile_prefab; // add a missile prefab object in inspector to it
	private float rotationSpeed; // to game master

	#region animation and FX related
	private SpriteRenderer spriteRenderer;
	private Animator animator;
	#endregion

	public void SetMoveVariables()
	{
		current_MaxSpeed = GameMaster.shipMaxSpeed;

		decelerationTime = 0.25f;
		acelerationNr1Time = 0.4f;
		acelerationNr2Time = 0.4f;
		Velocity_minPercent = 0.3f;
		Velocity_firstSegmentPart = 0.7f;
		acelerationNr1 = (Velocity_firstSegmentPart * current_MaxSpeed - Velocity_minPercent * current_MaxSpeed) / acelerationNr1Time;
		acelerationNr2 = (current_MaxSpeed - Velocity_minPercent * current_MaxSpeed - acelerationNr1Time * acelerationNr1) / acelerationNr2Time;


	}

	protected virtual void Awake()
	{
		SetMoveVariables ();
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		missileSpawnPointGO = gameObject.transform.Find("MissileSpawnPoint").gameObject ;
		GetSpawnMisslePoint ();
		missileVelocity = GameMaster.missileVelocity;
		rotationSpeed = GameMaster.rotationSpeed;
		gm = (GameMaster)FindObjectOfType<GameMaster> ();
		gameObject.transform.position = (Vector2)Camera.main.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 1.0f));
		gameObject.transform.rotation = Quaternion.Euler (new Vector3 (0.0f, 0.0f, 90.0f));


	}

	protected void Start()
	{
		#region animation ans SFX related
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
		#endregion
	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate ();
		//GetInputForces (); // use it with position update

		//Velocity for Debug
		VelocityX_look = m_Rigidbody2D.velocity.x;
		VelocityY_look = m_Rigidbody2D.velocity.y;

		//!!!!!!!!!!!!!!!
		Rotation ();

	}

	void Rotation()
	{
		if (InputMaster.left) {
			//Debug.Log ("rotate ship left");
			transform.Rotate(0,0, Time.deltaTime * rotationSpeed);
		}

		if (InputMaster.right) {
			//Debug.Log ("rotate ship right");
			transform.Rotate(0,0,  - Time.deltaTime * rotationSpeed);
		}
	}

	//HOW TO INTEGRATE UPDATE POSITION
	public virtual void Move(bool accelerationOn)
	{
		GetSpawnMisslePoint ();

		#region animation and fx related
		// ship engine FX
		animator.SetBool("EngineOn", accelerationOn); // change animator variable to false or true
		#endregion

	
		#region Engine Velocity Acceleration
		if (accelerationOn) {
			
			
			
			//Debug.Log ("acceleration in Move mthod in player ship");
			//VelocityImpuls for min Velocity 
			if (m_Rigidbody2D.velocity.magnitude < current_MaxSpeed * Velocity_minPercent) {
					
					m_Rigidbody2D.velocity = (missileSpawnPoint - (Vector2)transform.position).normalized * current_MaxSpeed * Velocity_minPercent  ; 
			}

			//First aceleration segment
			if (m_Rigidbody2D.velocity.magnitude >= current_MaxSpeed * Velocity_minPercent && m_Rigidbody2D.velocity.magnitude <= current_MaxSpeed * Velocity_firstSegmentPart) {
					//Debug.Log ("Segment 1 aceleration");
				m_Rigidbody2D.velocity = m_Rigidbody2D.velocity + (missileSpawnPoint - (Vector2)transform.position).normalized * acelerationNr1*Time.deltaTime;
			}

			//Second aceleration segment
			if (m_Rigidbody2D.velocity.magnitude >= current_MaxSpeed * Velocity_firstSegmentPart && m_Rigidbody2D.velocity.magnitude <= current_MaxSpeed) {
					//Debug.Log ("Segment 2 aceleration");
				m_Rigidbody2D.velocity = m_Rigidbody2D.velocity + (missileSpawnPoint - (Vector2)transform.position).normalized * acelerationNr2*Time.deltaTime;
			} else if (m_Rigidbody2D.velocity.magnitude >= current_MaxSpeed) {
				//Debug.Log("Max Speed");
				m_Rigidbody2D.velocity = (m_Rigidbody2D.velocity + (missileSpawnPoint - (Vector2)transform.position).normalized * acelerationNr2*Time.deltaTime).normalized * current_MaxSpeed;
				
			}
				
		/*
			// Max Velocity Blockade for movement
			if (m_Rigidbody2D.velocity.magnitude >= current_MaxSpeed) {
				m_Rigidbody2D.velocity = m_Rigidbody2D.velocity.normalized * current_MaxSpeed; 
			}
		*/

		#endregion
		} else if (accelerationOn == false) {
			#if DecelerationLessInertia
				//deceleration
				#region Code this deceleration for decelerating from the current velocity on a normal vector
				//Deceleration

				if (m_Rigidbody2D.velocity.magnitude > 0.2f) {
					//m_Rigidbody2D.velocity = m_Rigidbody2D.velocity - m_Rigidbody2D.velocity.normalized * current_MaxSpeed/0.50f * Time.deltaTime;
					m_Rigidbody2D.velocity = m_Rigidbody2D.velocity * 0.98f;
				}else if (m_Rigidbody2D.velocity.magnitude <= 0.2f)	{
					m_Rigidbody2D.velocity = new Vector2(0.0f,0.0f);
				}
				#endregion

			#endif
		}

	}


	public virtual void GetSpawnMisslePoint()
	{

		missileSpawnPoint = missileSpawnPointGO.transform.position;
	}
		
	public void SpawnMissleFunction()
	{
		
		//if (Input.GetKeyDown(KeyCode.Space)) { // spcae key input for shooting

				GetSpawnMisslePoint ();//Get Missile spawn position

				//Get Velocity for missiles
				Vector2 cVec;
				cVec = missileSpawnPoint - (Vector2)gameObject.transform.position;
				cVec.Normalize ();
				v2_missileSpeed = cVec * missileVelocity;

				//Spawn Missile, get a prefab
				GameObject missile;
				missile = (GameObject)Instantiate (missile_prefab, missileSpawnPoint, Quaternion.identity) as GameObject;
				missile.GetComponent<MissileBase> ().SetMissileVelocity (v2_missileSpeed);
				
				//Play SFX
				AudioMaster.Instance.PlaySFX (SFX_types.shot);
				
				//Spawn FX
				Vector3 shipOrientation = new Vector3 (gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, gameObject.transform.eulerAngles.z -90.0f);
				FXMaster.Instance.SpawnFX (FX_types.shotExplosion, missileSpawnPoint, shipOrientation, gameObject.transform);
		//}



		
	}

	public void GotHit()
	{
		Debug.Log ("Ship got hit");
		gm.LoseLiveUI ();

		Destroy (gameObject);
	}

}