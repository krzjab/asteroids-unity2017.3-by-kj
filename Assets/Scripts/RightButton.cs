﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightButton : ArrowButton {

	protected override void InputChange(bool isActive)
	{
		InputMaster.right = isActive;
	}

	protected override void Start()
	{
		base.Start ();
		transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.21f, 0.10f, 1.0f));
	}
}
