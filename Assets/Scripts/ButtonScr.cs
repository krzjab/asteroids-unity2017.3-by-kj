﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ButtonScr : MonoBehaviour {

	protected SpriteRenderer buttonSprite;


	// Use this for initialization
	protected virtual void Start () {
		buttonSprite = gameObject.GetComponent<SpriteRenderer> ();
	}
		
	protected virtual void OnMouseEnter()
	{
		buttonSprite.color = Color.red;
	}

	protected virtual void OnMouseDown()
	{
		buttonSprite.color = Color.blue;
	}

	protected virtual void OnMouseExit()
	{
		buttonSprite.color = Color.white;
	}

	protected virtual void OnMouseUpAsButton()
	{
		// what does the button do?
	}




}
