﻿//#define MonitorViewPortPos

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicObject : MonoBehaviour {
	

	protected virtual void FixedUpdate(){

		//UpdatePosition();
		EdgeTeleport();// monitoring nad teleportation of the gameobject in relation to the camera viewport
	}
		
	protected void EdgeTeleport()
	{
		//monitoring of gameobject position in relation to camera vieport or cameraspace
		Vector2 screenPosVP = Camera.main.WorldToViewportPoint (gameObject.transform.position);

		#if MonitorViewPortPos
		Vector2 screenPosPIX = Camera.main.WorldToScreenPoint (gameObject.transform.position);
		Debug.Log (gameObject.name + " is " + screenPosVP + " viewport coorinants from the left");
		Debug.Log (gameObject.name + " is " + screenPosPIX + " pixels from the left");
		#endif

		//teleport from left edge to right edge of vieport
		if (screenPosVP.x < 0.0f) 
		{
			gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (1.0f,screenPosVP.y, 1.0f)); 
		}

		//teleport from right edge to left edge of viewport
		if (screenPosVP.x > 1.0f) 
		{
			gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.0f,screenPosVP.y, 1.0f)); 
		}

		//teleport from upper edge to lower edge of viewport
		if(screenPosVP.y > 1.0)
		{
			gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3( screenPosVP.x, 0.0f, 1.0f));
		}

		//teleport from lower edge to upper edge of viewport
		if(screenPosVP.y < 0.0)
		{
			gameObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3( screenPosVP.x, 1.0f, 1.0f));
		}
	}



}
