﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBig : Asteroid {

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		asterTypeToSpawn = AsteroidSpawner.AsteroidType.Medium;
	}
	
	// Update is called once per frame
	protected override void FixedUpdate () {
		base.FixedUpdate ();
	}
}
