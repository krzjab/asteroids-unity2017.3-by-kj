﻿//#define DontDestroyOnLoad
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class SingletonScoreManager : MonoBehaviour {

	//Sttic singleton property
	public static SingletonScoreManager Instance {get; private set;} // instance of score manager

	//public property for manager

	public static int highScore;
	public static int score ;
	public int scoreForInspector;
	public string scoreTitle = "Score:";

	private GameObject highScoreTextGO;
	private GameObject scoreTextGO;


	public static void SaveScore()
	{
		PlayerPrefs.SetInt ("HighScore", (int)SingletonScoreManager.score);
	}

	public int LoadScore()
	{
		return PlayerPrefs.GetInt ("HighScore");
	}

	void Awake()
	{
		if (LoadScore () != 0) {
			highScore = LoadScore ();
		} else {
			//highScore = (int)0.0;
		}
		//Check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			//Detroy other instances if they are not the same
			Destroy(gameObject);
		}

		//Save our current singleton instance
		Instance = this;

		#if DontDestroyOnLoad
			//Make sure that the instance is not destroyed between scenes (this is optional)
			DontDestroyOnLoad(gameObject);
		#endif


	}

	//public method for manager
	public void AddPoints(int points)
	{
		score += points;
		if (scoreTextGO != null) {
			scoreTextGO.GetComponent<Text> ().text = "Score:"+ (int)score;
		}
	}

	// Use this for initialization
	void Start () {
		highScoreTextGO = GameObject.Find ("Canvas/HighScoreText");
		if (highScoreTextGO != null) {
			highScoreTextGO.GetComponent<Text> ().text = "High Score: " + highScore;
		}


		scoreTextGO = GameObject.Find ("Canvas/ScoreText");
		if (scoreTextGO != null) {
			scoreTextGO.GetComponent<Text> ().text = "Score: " + score;
		}
	}
	
	// Update is called once per frame
	void Update () {
		scoreForInspector = score;
	}
}
