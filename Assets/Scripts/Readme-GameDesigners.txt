Writen by Krzysztof Jab�o�ski:

Hello fellow Game Designer, this readme file is to make your life easier.

You can adjust game play by changing those constants (all of them are in Assets/Scripts/GameMaster.cs file):
Open provided Unity project and run this file "Assets/Scripts/GameMaster.cs". Now you can change the constants.


#region Constants for GameDesigner !!!!!!!!!!!!!!!
public const int maxLives = 3;
public const float shipMaxSpeed = 8.0f;
public const int scorePerAsteroid = 10;
public const float asteroidSpawnTime = 3.0f; //asteroids are spawning in time intervals
public const float velReductionAfterHit = 0.9f; // fraction of velocity of new spawned asteroids after a asteroid hit, Example: if 0.90 new asteroid has 90% velocity of the destroyed asteroid
public const float astrVeloMin = 3.0f; // asteroids minimal velocity on spawning from AsteroidSpawner
public const float astrVeloMax = 5.0f; // asteroids maximal velocity on spawning from AsteroidSpawner
public const float timeToDestroyMissile = 2.0f; // float -> seconds, after this time missile disappear
public const float missileVelocity = 11.0f; // velocity of new spawned missiles which are spawned by playerShip
public const float rotationSpeed = 200; // rotation speed of playerShip
#endregion